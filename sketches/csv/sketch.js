// parameters
let p = {
  boolean: true,
  numeric: 50,
  numericMin: 0,
  numericMax: 100,
  numericStep: 1,
}

function preload() {
  // local file (in data/" directory of this sketch)
  let fn = "data/ca_postal_codes.csv";

// remember, always good to print out some information about the 
  // data you loaded to help diagnose problems
  print(`Loading '${fn}'...`);
  table = loadTable(fn, 'csv', 'header', function() {
    print(`  loaded ${table.getRowCount()} rows in ${table.getColumnCount()} columns`);
  });
}

function setup() {
  
  // simple HTML textarea debug window
  createDebugWindow();
  createCanvas(500, 500);

  background(200)
  fill(0)

  // print out the column names and types (good for diagnosing problems)
  for (c of table.columns) {
    debug(`${c}: ${typeof(c)}`);
  }
  
  // try sorting the data
  //table.sort("Longitude");

  // iterate through the rows
  for (let i = 0; i < table.getRowCount(); i++) {
    let r = table.getRow(i);
    let la = r.getString("Latitude");
    let lo = r.getString("Longitude");
    let y = map(la, 0, 90, height, 0);
    let x = map(lo, -180, 0, 0, width);
    fill(0, 20)
    noStroke();
    circle(x, y, 3)
    // for (let i=0; i<10; i++) {
    //   x += (randomGaussian()-0.5)*2
    //   y += (randomGaussian()-0.5)*2
    //   circle(x, y, 2)
    // }
    debug(`${i} ${la} ${lo}`);
  }  
 }

function draw() {
}

function keyPressed() {
  if (key == ' ') {
  }
}

function mousePressed() {
}

function windowResized() {
}

// global callback from the settings GUI
function paramChanged(name) {
}



