// the model
let facemesh;
let facemask;
// latest model predictions
let predictions = [];
// video capture
let video;
let videoStill;
let emoji;
let index = 0;

function preload() {
  emoji = loadImage("data/emoji.png");
}

function setup() {
  createCanvas(640, 480);
  background(0, 0, 0);

  video = createCapture(VIDEO);

  // initialize the model
  const options = {
    detectionConfidence: 0.5,
  }
  facemesh = ml5.facemesh(video, options, modelReady);

  // This sets up an event that fills the global variable "predictions"
  // with an array every time new predictions are made
  facemesh.on("predict", results => {
    predictions = results;
  });

  facemask = createGraphics(width, height)

  // Hide the video element, and just show the canvas
  video.hide();
}

function modelReady() {
  console.log("Model ready!");
}

let canvas;

function drawEye(name1, name2, index) {
    let upperEye = predictions[index].annotations[name1];
    let lowerEye = predictions[index].annotations[name2];

    facemask.beginShape();
    for (let i = 0; i < lowerEye.length; i++) {
      let [x, y] = lowerEye[i];
      facemask.vertex(x, y);
    }

    for (let i = 0; i < upperEye.length; i++) {
      let [x, y] = upperEye[upperEye.length-1-i];
      facemask.vertex(x, y);
    }
    facemask.endShape(CLOSE);
}

function draw() {
  clear();

  facemask.noStroke();
  facemask.colorMode(HSB);
  facemask.fill(0, 0, 0, 0.03);
  facemask.rect(0, 0, width, height);
  
  if (predictions.length > 0) {
    for (let i=0; i<predictions.length; i++) {
      facemask.fill(0, 100, 100*randomGaussian(2));
      drawEye("rightEyeUpper1", "rightEyeLower2", i);
      drawEye("leftEyeUpper1", "leftEyeLower2", i);
    }
  }

  push();
    blendMode(MULTIPLY);
    translate(width, 0);
    scale(-1, 1);
    image(video, 0, 0, width, height);
    image(facemask, 0, 0, width, height);
  pop();

  
}