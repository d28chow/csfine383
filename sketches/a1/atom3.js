

class Atom1 extends Atom {
    s;
    d;

    constructor(index) {
        // this.osc = new p5.Oscillator("square");
        // this.env2 = new p5.Envelope(0.01, 0.1, 0.01, 0);
        // let f = 200;
        super(index);

        this.osc2 = new p5.Oscillator("triangle");
        this.env = new p5.Envelope(0.001, 0.1, 0.1, 0);
        this.osc.freq(200);

        this.x = random() * width;
        this.y = random() * height;

        // this.x = width/2;
        // this.y = height/2;
        this.s = 0;
        this.d = 50;
    }

    hit() {
        var t1 = gsap.timeline();
        //this.env.play(this.osc);
        t1.to(this, {
            s: 10,
            duration: 0.5,
            ease: "elastic.in",
            onComplete: () => {
                this.s = 1;
            }
        });
    }

    update(vChance) {
        if (random(1) < (vChance / 500) / frameRate()) {
            this.hit();
        }
    }

    draw() {
        // draw a line between last position
        // and current position
        if (this.l < 0.01) return;

        stroke(100, 20);
        noFill();
        const d = dist(this.x, this.y, this.cx, this.cy);
        this.drawRect(this.x, this.y, this.s * d/2 + this.index*random(), 0);
    }

    drawRect(x, y, s, i) {
        if (i > 3) {
            return;
        }

        // const d = dist(x, y, this.cx, this.cy);
        // for (let j=0; j<4; j++) {
        //     const dx = (randomGaussian()*d)/10;
        //     const dy = (randomGaussian()*d)/10;
        //     this.drawRect(x + dx, y + dy, s, i+1);
        // }

        let a = s / 4 + (5 - i) * 2 + random();
        let b = s / 2 + random()*3;

        if (this.index%2 == 0) {
            a = s/8 + random() + 10;
        }

        // if (this.index%3 == 0) {
        //     a = random()*10 + 10;
        // }

        // if (i%2 == 0) {
        //     fill(0);
        // }   else {
        //     fill(1);
        // }

        this.drawRect(x - a, y - a, b, i + 1);
        this.drawRect(x + a, y - a, b, i + 1);
        this.drawRect(x + a, y + a, b, i + 1);
        this.drawRect(x - a, y + a, b, i + 1);

        if (i != 0) rect(x - b, y - b, s);
    }
}