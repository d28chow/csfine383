

class Atom1 extends Atom {
    r;

    constructor(index) {
        // this.osc = new p5.Oscillator("square");
        // this.env2 = new p5.Envelope(0.01, 0.1, 0.01, 0);
        // let f = 200;
        super(index);

        this.osc2 = new p5.Oscillator("triangle");
        this.env = new p5.Envelope(0.001, 0.1, 0.1, 0);
        this.osc.freq(200);

        this.x = width/2;
        this.y = height/2;

        // this.x = width/2;
        // this.y = height/2;
        this.r = 5;
    }

    hit() {
        var t1 = gsap.timeline();
        //this.env.play(this.osc);
        t1.to(this, {
            r: width,
            duration: "0.2",
            ease: "elastic.out(1.5, 0.5)",
            onComplete: () => {
                this.r = 5;
            }
        });
    }

    update(vChance) {
        if (random(1) < (vChance / 500) / frameRate()) {
            this.hit();
        }
    }

    draw() {
        fill(100);
        noStroke();
        circle(this.x, this.y, this.r)
    }
}