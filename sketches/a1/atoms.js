class Atoms {
    atoms = [];
    vChance = 0;
    cx;
    cy;

    constructor() {
        this.atoms = [];

        for (let i=0; i<p.agentNum; i++) {
            this.atoms.push(new Atom1(i));
        }

        gsap.to(this, {
            vChance: 700 + random()*700,
            duration: 20,
            ease: "expo.in",
            repeatDelay: 5,
            repeat: -1,
            onComplete: () => {
                background(0);
            }
        });

    }

    draw() {
        
        this.atoms.forEach(atom => atom.draw());
    }

    update() {
        // this.atoms.forEach(atom => atom.update(this.vChance));

        if (random(1) < (this.vChance/50) / frameRate()) {
            const n = 1;
            const cx = Array.from({length: n}, () => randomGaussian(width/2, width/8));
            const cy = Array.from({length: n}, () => randomGaussian(height/2, height/8));

            // const cx = randomGaussian(width/2, width/8);
            // const cy = randomGaussian(height/2, height/8);

            this.atoms.forEach((atom, index) => {
                atom.setCenter(cx[index%n], cy[index%n])
                // atom.setCenter(cx, cy)
                atom.update(this.vChance);
            })
        
            this.atoms.forEach(atom => atom.update(501*frameRate()));
        }
        else {
            this.atoms.forEach(atom => atom.update(this.vChance));
        }

        // if (random(1) < (this.vChance/50) / frameRate()) {
        //     shuffle(this.atoms, true)
        //     this.atoms.forEach((atom, index) => {
        //         atom.index = index;
        //         atom.x = random()*width;
        //         atom.y = random()*width;
        //     });
        // }
        
        // if (this.vChance > 700) {
        //     this.atoms.forEach(atom => Atom.prototype.mute.call(atom))
        // }
    }
}