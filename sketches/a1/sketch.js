/*
 * Starter code for creating a generative drawing using agents
 */

// parameters
let p = {
  // toggle filling screen or not
  fillScreen: false,

  // number of agents
  agentNum: 100,
  agentNumMin: 10,
  agentNumMax: 1000,

}

let atoms;

function setup() {
  createCanvas(600, 600)
  colorMode(HSB, 100)

  // add params to a GUI
  createParamGui(p, paramChanged);

  createAtoms();
}

function draw() {
  background(0);

  // fill(0, 5);
  // rect(0, 0, width, height);

  atoms.update();
  atoms.draw();
}

// create the grid of agents, one agent per grid location
function createAtoms() {

  // setup the canvas
  if (p.fillScreen) {
    resizeCanvas(windowWidth, windowHeight)
  } else {
    resizeCanvas(600, 600)
  }

  // clear the background
  // background(0);  

  atoms = {};
  atoms = new Atoms();
}

function keyPressed() {
  // space to reset all agents
  if (key == ' ') {
    createAtoms();
  }
  // SHIFT-S saves the current canvas
  if (key == 'S') {
    save('canvas.png')
  }
}

// global callback from the settings GUI
function paramChanged(name) {
  if (name == "agentNum" || name == "fillScreen") {
    createAtoms();
  }
}