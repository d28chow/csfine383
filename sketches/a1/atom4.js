

class Atom1 extends Atom {
    l;
    s;

    constructor(index) {
        // this.osc = new p5.Oscillator("square");
        // this.env2 = new p5.Envelope(0.01, 0.1, 0.01, 0);
        // let f = 200;
        super(index);

        this.osc2 = new p5.Oscillator("triangle");
        this.env = new p5.Envelope(0.001, 0.1, 0.1, 0);
        this.osc.freq(200);

        this.x = random()*width;
        this.y = random()*height;

        // this.x = width/2;
        // this.y = height/2;
        this.l = 0;
        this.s = 1.0;
    }

    hit() {
        var t1 = gsap.timeline();
        //this.env.play(this.osc);
        t1.to(this, {
            l: width*2,
            duration: "random(0.1, 0.2)",
            ease: "",
            onComplete: () => {
                this.l = 0;
            }
        });
        this.s = (randomGaussian()+1)*2;
    }

    update(vChance) {
        if (random(1) < (vChance / 500) / frameRate()) {
            this.hit();
        }
    }

    draw() {
        if (this.l == 0) return;
        // draw a line between last position
        // and current position
        //if (this.l < 0.01) return;

        //const d = dist(this.x, this.y, this.cx, this.cy);
        fill(100, this.l%100+ 20);
        noStroke();
        
        let d = abs(this.y-this.cy);
        if (d < 10) {
            d = 50;
        }
        else {
            d = 0;
        }

        // const x = ceil(this.x/this.s) * this.s;

        const y = ceil(this.y/this.s) * this.s;

        if (this.l != 0) {
            for (let i=2; i<(this.s + d)*2; i++) {
                //rect(x - this.s/2, ((this.y + this.l) + i*random()*5) % height, this.s);
                rect((this.x + this.l + i*random()*10) % width, y - this.s/2, this.s);
            }
        }

        
        //rect(x, (this.y + this.l) % height, this.s);

        rect((this.x + this.l + random()*10) % width, y - this.s/2, this.s);
        
    }
}