

class Atom {
    x;
    y;
    cx;
    cy;
    theta;
    index;
    mute;

    osc;
    env;

    constructor(_index) {
        this.index = _index;
        this.mute = false;

        this.osc = new p5.Oscillator();
        this.env = new p5.Envelope(0.01, 0.1, 0.5, 0);

        let f = 200 + random()*500;
        f = floor(f / 100) * 100;

        this.osc.freq(f);
        this.osc.amp(0);
        this.osc.start();

        // this.cx = width/2;
        // this.cy = height/2;
    }

    setCenter(_x, _y) {
        this.cx = _x;
        this.cy = _y;
    }

    mute() { this.mute = true }
    unMute() { this.mute = false; }
}