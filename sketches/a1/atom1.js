

class Atom1 extends Atom {

    constructor(index) {
        super(index);

        this.r1 = 0;
        this.x = random()*width;
        this.y = random()*height;
    }

    hit(vChance) {
        var t1 = gsap.timeline();
        this.env.play(this.osc);
        
        t1.to(this, {
            r1: width*2,
            duration: 1,
            ease: "expo.in",
            onComplete: () => {
                // this.r1 = 0;
            }
        });
        
    }

    update(vChance) {
        if (random(1) < (vChance/500) / frameRate()) {
            this.hit(vChance);
        }
    }

    draw() {
        if (this.r1 < 1) return;
        // draw a line between last position
        // and current position
        stroke(100);
        strokeWeight(0.5);
        noFill();
        circle(this.x, this.y, this.r1);
        noStroke();
    }
}