

class Atom1 extends Atom {
    l;

    constructor(index) {
        // this.osc = new p5.Oscillator("square");
        // this.env2 = new p5.Envelope(0.01, 0.1, 0.01, 0);
        // let f = 200;
        super(index);

        this.osc2 = new p5.Oscillator("triangle");
        this.env = new p5.Envelope(0.01, 0.1, 0.1, 0);
        this.osc.freq(200)

        this.x = random()*width;
        this.y = random()*height;

        this.cx = width/2;
        this.cy = height/2;
        this.l = 0;
    }

    hit() {
        if (!this.mute) this.env.play(this.osc);
        
        gsap.to(this, {
            l: 1.0,
            duration: 0.2,
            ease: "elastic.in",
            onComplete: () => {
                this.l = 0;
                // this.osc.amp(0);
            }
        });
        
    }

    update(vChance) {
        if (random(1) < (vChance/500) / frameRate()) {
            this.hit();
        }
    }

    draw() {
        // draw a line between last position
        // and current position
        if (this.l < 0.01) return;

        strokeWeight(5);
        stroke(0);
        // let x1 = this.x2*(1-this.l) + this.x1*this.l;
        // let y1 = this.y2*(1-this.l) + this.y1*this.l;
        
        let x2 = this.x*(1-this.l) + this.cx*this.l;
        let y2 = this.y*(1-this.l) + this.cy*this.l;

        line(this.x, this.y, x2, y2);

        strokeWeight(0.5);
        stroke(100);
        line(this.x, this.y, x2, y2);
        noStroke();
    }
}