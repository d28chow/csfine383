// the model
let facemesh;
let facemask;
// latest model predictions
let predictions = [];
// video capture
let video;
let textStill;

let all;
let words;

function preload() {
  let src = "data/1342-0.txt";

  let lines = loadStrings(src, function () {
    let s = ''
    for (l of lines) {
      s += l + " "
    }

    words = splitTokens(s, " ");

    all = lines.join('\n');
  });
}

function setup() {
  createCanvas(640, 480);
  pixelDensity(1)

  video = createCapture(VIDEO);
  video.width = width;
  video.height = height;

  textStill = createGraphics(width, height)

  video.hide();
}

let x = 0
let y = 0
let i = 0
let index = 0
let size = 10

function draw() {
  clear();
  textStill.background(0)
  textStill.fill(0);
  textStill.textSize(size);
  textStill.textWrap(WORD);
  textStill.textAlign(LEFT, TOP);

  blendMode(MULTIPLY);

  
  // loadPixels();
  while (y < height) {
    let w = textWidth(words[i])
    
    textStill.fill(i%255 + 100);
    textStill.text(words[i], x, y, w);
    x += w
    if (x > width) {
      y += size
      x = 0
    }
    i++;
    
  }
  i = index++;
  i %= words.length-100
  x = 0
  y = 0
  // updatePixels();
  // background(0);
  

  push();
    translate(width, 0);
    scale(-1, 1);
    image(video, 0, 0, width, height);
  pop();

  image(textStill, 0, 0, width, height);

  blendMode(BLEND);
}