let facemesh
let posenet
let prediction

function setup() {
  createCanvas(1080, 1080)
  angleMode(DEGREES)
  noCursor()

  facemesh = new FaceMesh()
}

function radialLines(x, y, theta, step) {
  strokeWeight(5)
  stroke(0)

  if (theta > 360) return
  line(x, y, width * sin(theta) + width / 2, height * cos(theta) + height / 2, 10)
  radialLines(x, y, theta + step, step)
}

function radialCircles(x, y, size, step) {
  strokeWeight(0.5)
  stroke(size)

  if (size < 0) return
  circle(x, y, size)
  radialCircles(x, y, size - step, step)
}

function entity1(x, y, options) {
  noFill()

  radialCircles(x, y, width / 2, map(options.d, 0, 500, 255, 1, true));
  // radialCircles(x, y, width/2, 2);
  // radialLines(x, y, 0, 10);
}

function entity2(x, y, options) {
  fill(255)
  noStroke()
  let s = options.d
  rect(x - s/2, y - s/2, s)
}

function entity3(x, y, options) {
  entity4(0, width/2, 10)
}

function entity4(theta, size, step) {
  if (size < 0) return

  let r = size

  curveVertex(r * sin(theta) + width / 2, r * cos(theta) + height / 2)
  if (theta > 360) {
    // curveVertex(r * sin(theta) + width / 2, r * cos(theta) + height / 2)
    endShape(CLOSE);
    beginShape();
    entity4(0, size-step, step)
  }
  else {
    entity4(theta+step, size, step)
  }
}

function draw() {
  background(255);
  
  // specularColor(255, 0, 0);
  // pointLight(255, 0, 0, 0, 0, 50 + 10*noise(frameCount));
  // noFill()
  // strokeWeight(2);
  // stroke(255);
  // beginShape();
  // entity4(0, width/3, 10)

  facemesh.draw(options => {
    // entity1(width / 2, height / 2, options);
  });

  // push()
  // scale(-1, 1)
  // translate(-width, 0)
  // if (p.landmarks) drawLandmarks()
  // pop();


}




