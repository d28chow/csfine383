class FaceMesh {
    fps = 0
    face
    debug = false

    constructor(debug = false) {
        this.debug = debug

        let constraints = {
            video: {
                deviceId: {
                    exact: "0bea9dc66a9f6c82c22fdff7adc283e81b40be3448313c877737b4051e67ce36"
                },
                width: 1080,
                height: 1080,
            }
        };

        let video = createCapture(constraints);
        // video.hide();

        ml5.facemesh(video, {
            maxFaces: 1,
            detectionConfidence: 0.5,
        }).on("predict", results => {
            this.face = results[0];
        });
    }

    lx = width / 2
    ly = height / 2

    rx = width / 2
    ry = height / 2

    easing = (n) => -(cos(PI * n) - 1) / 4;

    ease(currX, currY, targetX, targetY) {
        const deltaX = targetX - currX
        const deltaY = targetY - currY

        return [currX + deltaX * this.easing(deltaX), currY + deltaY * this.easing(deltaY)]
    }

    draw(drawCb) {
        if (!this.face) return;
        const [t_lx, t_ly] = this.face.annotations['leftCheek'][0]
        const [t_rx, t_ry] = this.face.annotations['rightCheek'][0]

        const [lx, ly] = this.ease(this.lx, this.ly, t_lx, t_ly);
        const [rx, ry] = this.ease(this.rx, this.ry, t_rx, t_ry);

        const x = (lx + rx) / 2
        const y = (ly + ry) / 4

        let d = dist(lx, ly, rx, ry) * 2.8
        let a = atan2(ly - ry, lx - rx)

        this.lx = lx
        this.ly = ly
        this.rx = rx
        this.ry = ry

        push()
        imageMode(CENTER)
        translate(x, height - y - 300)
        scale(-1, 1)
        // translate(x-width, y)
        rotate(a)
        // image(this.video, 0, 0, width, height) // draw image at 0,0
        translate(-width / 2, -height / 2)
        drawCb({ d });
        this.drawEntity(d)
        if (this.debug) this.drawLandmarks()
        pop()
        if (this.debug) this.drawFps()

    }

    drawFps() {
        let a = 0.01
        this.fps = a * frameRate() + (1 - a) * this.fps
        fill(255)
        textAlign(LEFT, TOP)
        textSize(20.0)
        text(this.fps.toFixed(1), 10, 10)
    }

    drawLandmarks() {
        noStroke()
        fill(255);
        const keypoints = this.face.mesh;

        for (let k of keypoints) {
            const [x, y] = k;

            circle(x, y, 2);
        }
    }

    drawEntity(d) {
        noStroke()
        const keypoints = this.face.mesh
        const l = keypoints.length

        for (let i in keypoints) {
            let theta = (i / l) * 360
            let [_x, _y, _z] = keypoints[i]
            let t = millis() / 50
            let r = _x * sin(theta + t) + _y * cos(theta + t)

            r += d

            const x1 = r * sin(theta) + width / 2
            const y1 = r * cos(theta) + height / 2

            const x2 = _x * sin(theta + x1) + width / 2
            const y2 = _y * cos(theta + y1) + height / 2

            fill(255, 0, 0, 50);
            circle(x1, y1, _z)

            fill(0, 50);
            circle(x2, y2, _z)

        }
    }
}

/* list of annotations:
silhouette
lipsLowerOuter
lipsUpperOuter
lipsUpperInner
lipsLowerInner
rightEyeUpper0
rightEyeLower0
rightEyeUpper1
rightEyeLower1
rightEyeUpper2
rightEyeLower2
rightEyeLower3
rightEyebrowUpper
rightEyebrowLower
leftEyeUpper0
leftEyeLower0
leftEyeUpper1
leftEyeLower1
leftEyeUpper2
leftEyeLower2
leftEyeLower3
leftEyebrowUpper
leftEyebrowLower
midwayBetweenEyes
noseTip
noseBottom
noseRightCorner
noseLeftCorner
rightCheek
leftCheek
*/