

class Agent {

    // current position
    x;
    y;
    theta;
    // previous position
    px;
    py;
    i;

    constructor(_theta) {
        this.x = width / 2;
        this.y = height / 2;
        this.theta = _theta
        this.i = 0
    }

    thetaAvg() {
        let thetaAvg = 0;
        let count = 0;
        for (const agent of agents) {
            if (agent == this) continue;
            let d = dist(this.x, this.y, agent.x, agent.y);
            if (d < 10) {
                this.theta += random();
                noStroke(0);
                fill(65, 100, 100, (p.speed/100)*10);
                circle(this.x, this.y, 10);
                return 1;
            }
            
        }
        return 0;
    }

    update() {
        // save last position
        this.px = this.x;
        this.py = this.y;

        // pick a new position    
        this.x = this.x + sin(this.theta)*(p.speed/20);
        this.y = this.y + cos(this.theta)*(p.speed/20);

        this.theta = (this.theta + (random()-0.5)*p.chaos/10);

        if (this.x < 0 || this.x > width) {
            this.theta = -this.theta;
        }
        if (this.y < 0 || this.y > height) {
            this.theta -= 90;
        }
        this.i += 0.1;
    }

    draw() {
        // draw a line between last position
        // and current position
        strokeWeight(1);
        stroke(this.thetaAvg()*65, 100, 100, 80);
        line(this.px, this.py, this.x, this.y);
    }
}