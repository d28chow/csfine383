/*
 * Starter code for creating a generative drawing using agents
 */

// parameters
let p = {
  // toggle filling screen or not
  fillScreen: false,

  // number of agents
  agentNum: 100,
  agentNumMin: 10,
  agentNumMax: 500,

  // add more params here ...
  trail: 50, 
  speed: 30,
  chaos: 10

}

// list of agents
let agents

function setup() {
  createCanvas(600, 600)
  colorMode(HSB, 100)

  // add params to a GUI
  createParamGui(p, paramChanged);

  // load last params
  // s = getItem("params")

  // setup the window and create the agents
  createAgents()
}

function draw() {

  // update all the agents
  for (a of agents) {
    a.update();
  }

  // draw all the agents
  for (a of agents) {
    a.draw();
  }

  noStroke();
  const trail = 1 - p.trail/100;
  const speed = (p.speed/100) * 50;
  fill(100, trail*speed);
  rect(0, 0, width, height);
}

// create the grid of agents, one agent per grid location
function createAgents() {

  // setup the canvas
  if (p.fillScreen) {
    resizeCanvas(windowWidth, windowHeight)
  } else {
    resizeCanvas(600, 600)
  }

  // clear the background
  background(250);  

  // clear agent list
  agents = [];

  // create Agents
  for (i = 0; i < p.agentNum; i++) {
    let a = new Agent(360 * i/p.agentNum);
    agents.push(a);
  }
}

function keyPressed() {
  // space to reset all agents
  if (key == ' ') {
    createAgents();
  }
  // SHIFT-S saves the current canvas
  if (key == 'S') {
    save('canvas.png')
  }
}

function windowResized() {
  createAgents()
}

// global callback from the settings GUI
function paramChanged(name) {
  if (name == "agentNum" || name == "fillScreen") {
    createAgents()
  }
}