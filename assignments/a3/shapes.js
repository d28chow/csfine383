function wave(x, w, t, f, boxColor = -1) {
    const numDots = 20

    canvas.beginShape()

    canvas.curveVertex(x, -height / 2)
    for (let dot = 0; dot < numDots + 1; dot++) {
        f(x + sin(dot / numDots * width + t) * w, dot / numDots * height - height / 2)
    }
    canvas.curveVertex(x, height / 2)

    canvas.endShape()

    if (boxColor != -1) {
        canvas.noFill()
        canvas.stroke(boxColor, 100, 100)
        canvas.rect(x - w, -height / 2, w * 2, height)
    }
}

function star(x, y, outerR, innerR, f, boxColor = -1) {
    const numPoints = 10

    canvas.beginShape(TESS)

    for (let point = 0; point < numPoints; point++) {
        const theta1 = point / numPoints * 360
        const theta2 = theta1 + ((1 / numPoints) * 360) / 2

        // vertex(x + outerR*sin(theta1), y + outerR*cos(theta1))
        // vertex(x + innerR*sin(theta2), y + innerR*cos(theta2))
        f(x + outerR * sin(theta1), y + outerR * cos(theta1))
        f(x + innerR * sin(theta2), y + innerR * cos(theta2))
    }

    // f(x + outerR, y + outerR)

    canvas.endShape(CLOSE)

    if (boxColor != -1) {
        canvas.noFill()
        canvas.stroke(boxColor, 100, 100)
        canvas.strokeWeight(4)
        canvas.rect(x - outerR, y - outerR, outerR * 2, outerR * 2)
    }
}

function sprinkles(x, y, spread, f, boxColor = -1) {
    const numSprinkles = 10

    let minVal = [-width/2, height]
    let maxVal = [0, 0]

    for (let point = 0; point < numSprinkles; point++) {
        const theta = point / numSprinkles * 360
        const r = randomGaussian() * spread

        minVal = [min(r, minVal[0]), min(r, minVal[1])]
        maxVal = [max(r, maxVal[0]), max(r, maxVal[1])]

        f(x + r * sin(theta), y + r * cos(theta))
    }

    if (boxColor != -1) {
        canvas.noFill();
        canvas.stroke(boxColor, 100, 100)
        canvas.rect(x - maxVal[0], y - maxVal[1], maxVal[0] - minVal[0], maxVal[1] - minVal[1])
    }
}