let canvas, cam, master

function setup() {
  createCanvas(1280, 720);
  setAttributes("alpha", true)
  colorMode(HSB)
  frameRate(60)
  angleMode(DEGREES)
  noCursor()

  canvas = createGraphics(width, height, WEBGL)
  canvas.setAttributes("alpha", true);
  canvas.colorMode(HSB)
  canvas.angleMode(DEGREES)
  cam = canvas.createCamera()

  master = createTimeline()
  
}

function keyPressed() {
  if (keyCode == ENTER) {
    if (master.paused()) {
      master.play()
    }
    else {
      master.pause()
    }
  }
}

function debug() {
  background(0)
  const s = 10
  fill(100)
  rect(0, 0, s)
  rect(0, height-s, s)
  rect(width-s, 0, s)
  rect(width-s, height-s, s)

  rect(0, 0, width, height)
}

// function drawShapes() {

//   const t = millis() / 10


//   //wave(width/2, 100*sin(t/100), t/10)
//   const f1 = (x, y) => canvas.vertex(x, y)
//   const f2 = (x, y) => star(x, y, 100, 0, 0, f1, 0)
//   const f4 = (x, y) => canvas.curveVertex(x, y)
//   const f3 = (x, y) => wave(x, 20, t, f2)


//   const f6 = (x, y) => wave(x, 500, t, f4)

//   const f5 = (x, y) => sprinkles(x, y, 20, f6)

//   // star(width / 2, height / 2, 100, t, f3)
//   // canvas.push();
//   // canvas.rotateZ(0.01);

//   // canvas.rotateX(0.01);
//   // cam.perspective(sin(t) * 10)
//   // canvas.strokeWeight(5)
//   // canvas.stroke(100, 100, 100)
//   // canvas.noFill()

//   // // canvas.push()
//   // // // canvas.rotate(90)
//   // // f5(0, 0)
//   // // // star(0, 0, 100, 50*sin(t/100), t/100, f3)

//   // // canvas.pop()

//   // canvas.stroke(100)
//   // // f6(0, 0)
//   // star(0, 0, sin(t/10)*500, 0, 0, f2)
//   // canvas.pop();

//   // f5(sin(t)*width/2, 0)
//   // canvas.fill(0)
//   // canvas.push()
//   // canvas.scale(-1, 0)
//   // canvas.pop()

//   // canvas.line(0, -height/2, sin(t)*width/2, height/2)

  
// }
