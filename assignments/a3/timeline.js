const curveVertex = (x, y) => canvas.curveVertex(x, y)
const vertex = (x, y) => canvas.vertex(x, y)

function createTimeline() {

    let s = new Sound()

    let master = gsap.timeline({
        onUpdate: () => {
            this.t = millis()
            image(canvas, 0, 0)
            canvas.clear()
        }
    })

    let s1 = gsap.timeline()
    let s2 = gsap.timeline()
    let s3 = gsap.timeline()
    let s4 = gsap.timeline()

    s1.add(gsap.fromTo(this, { r1: 0, r2: 0, rand: 0, x: 50, y: 50, bg: 100 }, {
        keyframes: [
            { r1: 50, duration: 10, delay: 20, ease: "power1.in" },
            { x: width/2 - 10, duration: 20, ease: "sine.inOut" },
            { x: 0, y: 0, duration: 20, ease: "sine.inOut" },
            { r2: 50, rand: 100, delay: -10, duration: 10, ease: "sine.in" },
            { rand: 10, duration: 10, ease: "sine.out", bg: 0.01 },
            { x: -width/2 + 50, y: 0, rand: 0, duration: 20, ease: "sine.inOut" },
            { y: -height/2 + 50, duration: 10, ease: "sine.inOut" },
            { x: 0, y: 0, duration: 15, ease: "sine.inOut", rand: 100 },
            { r1: 0, duration: 10, ease: "sine.inOut", rand: 0, bg: 1 },
            // { x1: 0, y1: -100, duration: 2, delay: 2 },
        ],
        onUpdate: () => {
            // canvas.noFill()
            background(0, this.bg)
            cam.lookAt(random()*this.rand, random()*this.rand, random()*this.rand)
            canvas.strokeWeight(10)
            canvas.noStroke()
            canvas.fill(50, 100, 100)
            canvas.circle(this.x, this.y, this.r1)
            canvas.fill(0, 100, 100)
            canvas.circle(-this.x + this.rand, -this.y + this.rand, this.r2)
            // sprinkles(0, 0, 100, (x, y) => circle(x, y, 10))
        }
    }))

    s2.add(gsap.fromTo(this, { x: "random(" + -width/2 + "," + width/2 + ")", s: 100, rand: 0, f: 0 }, {
        repeat: 4,
        repeatRefresh: true,
        keyframes: [
            { s: 0, duration: 5, ease: "sine.out", rand: 20 },
            { f: 100, duration: 2, delay: -4, ease: "power2.out" },
            { f: 0, duration: 2 },
        ],
        onUpdate: () => {
            canvas.background(0)
            canvas.strokeWeight(10)
            canvas.noFill()
            canvas.stroke(this.s)

            wave(this.x, 10, this.rand*20, curveVertex)

            canvas.noStroke()

            canvas.fill(0, 100, this.f)

            sprinkles(this.x, sin(this.t/20)*height/2, this.f, (x, y) => canvas.circle(x, y, this.f/2))
        }
    }))

    s3.add(gsap.fromTo(this, { bg: 100, r1a: 0, r2a: 5, r1b: 0, r2b: 0, r1c: 0, r2c: 0, r1d: 0, r2d: 0}, {
        keyframes: [
            { r1a: 100, duration: 20, ease: "sine.in" },
            { r2a: 30, duration: 5, delay: -5, ease: "sine.inOut", bg: 0 },
            { r2a: 0, r1b: 100, r2b: 10, duration: 10, ease: "sine.inOut", bg: 100 },
            { r2b: 80, r1c: 100, r2c: 10, duration: 10, ease: "sine.inOut", bg: 0 },
            { r2c: 80, r2b: 10, duration: 10, ease: "sine.inOut", bg: 100 },
            { r2a: 10, r2c: 10, r1c: 80, r1d: 100, r2d: 10, duration: 20, ease: "sine.inOut", bg: 0 },
            { r2d: 80, duration: 20, ease: "sine.inOut" },
            { r1a: 0, r2a: 0, r1b: 0, r2b: 0, r1c: 0, r2c: 0, r1d: 0, r2d: 0, duration: 30, ease: "expo.inOut", bg: 0.5 },
        ],
        onUpdate: () => {
            const d = 200
            const t = (offset) => ((sin(this.t/200 + offset) + 1)/2)*360
            canvas.noStroke()
            canvas.fill(100)
            background(0, this.bg)
            canvas.rotateZ(0.1)
            // canvas.rotateX(0.1)
            // canvas.rotateY(0.1)
            star(0, 0, this.r1a, this.r2a, vertex, t(0))
            canvas.fill(30, 100, 100)
            star(d, 0, this.r1b, this.r2b, vertex, t(50))
            canvas.fill(200, 100, 100)
            star(-d, 0, this.r1c, this.r2c, vertex, t(100))


            canvas.fill(t(0), 100, 100)
            star(0, d, this.r1d, this.r2d, vertex, t(210))
            canvas.fill(t(0), 100, 100)
            star(d, d, this.r1d, this.r2d, vertex, t(220))
            canvas.fill(t(0), 100, 100)
            star(-d, d, this.r1d, this.r2d, vertex, t(230))
            canvas.fill(t(0), 100, 100)
            star(0, -d, this.r1d, this.r2d, vertex, t(240))
            canvas.fill(t(0), 100, 100)
            star(d, -d, this.r1d, this.r2d, vertex, t(250))
            canvas.fill(t(0), 100, 100)
            star(-d, -d, this.r1d, this.r2d, vertex, t(260))

        }
    }))

    // s4.add(gsap.fromTo(this, { fadeIn: 0, a: 0 }, {
    //     keyframes: [
    //         { fadeIn: 100 , duration: 1 },
    //         { a: 500, duration: 20, yoyo: true, repeat: -1}
    //     ],
    //     onUpdate: () => {
    //         canvas.strokeWeight(10)
    //         canvas.noStroke()
    //         canvas.fill(50, 100, 100)
    //         circle(0, 0, 10)
    //     }
    // }))

    return master.add(s1).add(s2).add(s3)
    // master.timeScale(3)
    
}