class Sound {
  osc
  filter
  noise
  env
  reverb
  dist
  start

  constructor() {
    this.env = new p5.Envelope(0, 6, 5, 0)
    this.reverb = new p5.Reverb(3, 1)
    this.filter = new p5.LowPass()
    this.filter.set(400, 50)
    this.filter.amp(1)

    this.dist = new p5.Distortion(0.25)
    this.noise = new p5.Noise('brown')
    this.noise.disconnect()

    this.filter.chain(this.reverb)

    this.noise.connect(this.filter)
    this.start = false

    // 

    // osc = new p5.Oscillator(50, 'square')
    // osc.amp(0.5)
    // osc.start()
  }

  toggle() {
    // this.start ? this.noise.stop() : this.noise.start()
    // this.start = !this.start
    this.noise.start()
    this.env.play(this.noise)
  }
}