let facemesh

function setup() {
  frameRate(30) // cap the frameRate at 30, any higher and it will feel too smooth
  createCanvas(1920, 1080)
  angleMode(DEGREES)
  noCursor()
  
  facemesh = new FaceMesh() // init video and start getting face data
}

function draw() {
  background(255, 10) // white background with opacity
  facemesh.draw() // process the face data and draw the sketch to the canvas
}