class FaceMesh {
    fps = 0
    debug = false
    annotations // left and right cheek positions
    keypoints // face landmarks
    lCheek
    rCheek

    constructor(debug = false) {
        this.debug = debug
        // init cheek position values, to be changed once facemesh starts
        this.lCheek = createVector(width / 2, height / 2)
        this.rCheek = createVector(width / 2, height / 2)

        // init video from external camera
        let video = createCapture({
            video: {
                // use the external USB camera, not laptop webcam
                deviceId: "0bea9dc66a9f6c82c22fdff7adc283e81b40be3448313c877737b4051e67ce36",
                width: 1920,
                height: 1080,
            }
        })
        video.hide()

        // detect face using camera feed
        ml5.facemesh(video, {
            maxFaces: 1,
            detectionConfidence: 0.5,
        }).on("predict", results => {
            if (!results.length) return
            // save data to be used in draw()
            this.annotations = results[0].annotations // left and right cheek
            this.keypoints = results[0].mesh // facial landmarks
        });
    }

    // sin easing function
    ease(v, delta) {
        const n = -(cos(PI * delta.mag()) - 1) / 4;
        delta.mult(n)
        v.add(delta)
    }

    draw() {
        if (!this.annotations) return;
        // get the change in cheek position compared to the previous frame
        const deltaL = createVector(...this.annotations['leftCheek'][0]).sub(this.lCheek)
        const deltaR = createVector(...this.annotations['rightCheek'][0]).sub(this.rCheek)

        // update cheek value according to easing
        this.ease(this.lCheek, deltaL)
        this.ease(this.rCheek, deltaR)


        let d = p5.Vector.dist(this.lCheek, this.rCheek) * 3 // get the face distance from the camera
        d = map(d, 100, 200, 200, 600) // remap depending on location

        // show the distance
        if (this.debug) {
            fill(255)
            rect(0, 0, 200, 50)
            fill(0)
            textAlign(LEFT, TOP)
            textSize(20.0)
            text(d, 10, 10)
        }

        // get the face tilt
        const diff = p5.Vector.sub(this.lCheek, this.rCheek)
        const a = atan2(diff.y, diff.x)

        // get the location of the center of the face
        const midPoint = p5.Vector.add(this.lCheek, this.rCheek).div(2, 4)

        // horizontally flip the video feed and position the sketch in the center
        push()
        imageMode(CENTER)
        translate(midPoint.x, height - midPoint.y - 200)
        scale(-1, 1)
        rotate(a)
        translate(-width / 2, -height / 2)
        this.drawSketch(d, a)
        if (this.debug) this.drawLandmarks()
        pop()
        if (this.debug) this.drawFps()

    }

    // gets the vertex to be used as part of a curved line
    getVertex(v, theta, d, t) {
        const r = v.x * sin(t) + v.y * cos(t) + d

        const dx = r * sin(theta) / 3
        const dy = r * cos(theta) / 3

        const x = v.x * sin(theta + dx) + width / 2
        const y = v.y * cos(theta + dy) + height / 2

        return [x, y]
    }

    // uses facial data to draw curved lines on the canvas
    drawSketch(d, a) {
        stroke(0, 25)
        strokeWeight(0.5)
        fill(255, 5);
        const t = frameCount / 700

        for (let i in this.keypoints) {
            const theta = (i / this.keypoints.length) * 360
            const kpVector = createVector(...this.keypoints[i]).add(d / 2, d / 2) // position of face landmark

            // draw a single curved line
            beginShape();
            // generates vertices of the curved line
            for (let j = -2; j < 2; j++) {
                // vertex is a function of the facial landmark mapping, vertex index, face distance and tilt, keypoint index, and time
                const vertex = this.getVertex(kpVector, theta * a / 10 + j * 20, d, t)
                curveVertex(...vertex)
            }
            endShape();
        }
    }

    // debug methods

    drawFps() {
        let a = 0.01
        this.fps = a * frameRate() + (1 - a) * this.fps
        fill(0)
        textAlign(LEFT, TOP)
        textSize(20.0)
        text(this.fps.toFixed(1), 10, 10)
    }

    drawLandmarks() {
        noStroke()
        fill(0);

        for (let k of this.keypoints) {
            const [x, y] = k;
            circle(x, y, 2);
        }
    }
}