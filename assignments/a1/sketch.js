let atoms, timeline;

function setup() {
  frameRate(60);
  noCursor();
  createCanvas(600, 600);
  colorMode(HSB, 100);
}

function draw() {
  if (!atoms) return;
  background(0);
  
  atoms.update();
  atoms.draw();
}

function createAtoms() {
  atoms = {};
  atoms = new Atoms();
  createTimeline(atoms);
}

function keyPressed() {
  if (key == ' ') {
    createAtoms();
  }

  if (key == 's') {
    save('canvas.png')
  }
}