function createTimeline(atoms) {
    let master = gsap.timeline();

    let t1 = gsap.timeline();
    let t2 = gsap.timeline();
    let t3 = gsap.timeline();
    let t4 = gsap.timeline({ repeat: 2 });
    let t5 = gsap.timeline();
    let t6 = gsap.timeline();

    t1.to(atoms.options, {
        onStart: () => {
            atoms.options.MODE = 1;
        },
        hitChance: 700 + random()*700,
        duration: 50,
        ease: "expo.in",
        onComplete: () => {
            if (master.reversed()) {
                master.seek("end");
            }
        }
    }).to(atoms.options, {
        onStart: () => {
            atoms.options.MODE = 2;
        },
        duration: 5,
        ease: "",
        onUpdate: () => {
            if (random(1) < 0.1) {
                _shuffle(atoms);
            }
        }
    });

    t2.fromTo(atoms.options, {hitChance: 0}, {
        onStart: () => {
            atoms.options.MODE = 0;
            atoms.options.amp = 0.1;
        },
        hitChance: 700 + random()*700,
        duration: 30,
        ease: "expo.in",
        onUpdate: () => {
            if (random(1) < 1 / frameRate()) {
                _shuffle(atoms);
                atoms.options.MODE = 2;
            }
            if (random(1) < 0.05) {
                atoms.options.MODE = 0;
            }
        }
    });

    t3.to(atoms.options, {
        onStart: () => {
            atoms.options.MODE = 2;
            atoms.options.amp = 0;
        },
        duration: 5,
        onUpdate: () => {
            if (random(1) < 0.5) {
                _shuffle(atoms);
            }
        },
        repeatDelay: 5
    });

    t4.to(atoms.options, {
        onStart: () => {
            atoms.options.MODE = 3;
            atoms.options.hitChance = 10000;
            atoms.options.amp = 0.05
        },
        duration: 2
    }).to(atoms.options, {
        onStart: () => {
            atoms.options.MODE = 5;
            atoms.options.amp = 0
        },
        duration: 1
    });

    t5.fromTo(atoms.options, {x: 0}, {
        x: 0.9,
        onStart: () => {
            atoms.options.hitChance = 1000 * frameRate();
            atoms.options.amp = 0.1;
        },
        onUpdate: () => {
            if (random(1) < atoms.options.x) {
                atoms.options.MODE = int(random(0, 5));
            }
        },
        ease: "rough",
        duration: 10,
    }).to(atoms.options, {
        onStart: () => {
            atoms.options.amp = 0.1;
            atoms.options.MODE = 2;
        },
        duration: 15
    });

    t6.to(atoms.options, {
        onStart: ()=> {
            atoms.options.MODE = 4;
            atoms.options.amp = 0;
        },
        duration: 5
    });
    
    master.add(t1).add(t2).add(t3).add(t4).add(t5).add(t6);

    return master;
}

function _shuffle(atoms) {
    shuffle(atoms.atoms, true)
    atoms.atoms.forEach((atom, index) => {
        atom.index = index;
        atom.x = random()*width;
        atom.y = random()*width;
    });
}