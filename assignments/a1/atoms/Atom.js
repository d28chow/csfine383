class Atom {
    x;
    y;
    cx;
    cy;
    size;

    index;
    osc;
    delta;

    constructor(_index, filter) {
        this.index = _index;

        this.osc = new p5.Oscillator();
        this.osc.disconnect();
        this.osc.connect(filter);
        this.osc.amp(0);
        this.osc.start();

        this.x = random() * width;
        this.y = random() * height;

        this.cx = width / 2;
        this.cy = height / 2;

        this.size = 0;
        this.delta = 0;
    }

    update(options) {
        if (this.exit(options.MODE)) return;
        if (random(1) > (options.hitChance / 500) / frameRate()) return

        let onHit = {};
        this.osc.amp(options.amp);
        let f = 200;
        let t = "sine";

        switch (options.MODE) {
            case 0: // dash
                if (options.amp != 0) this.osc.amp(0.03);
                onHit = {
                    delta: width * 2,
                    duration: "random(0.1, 0.2)",
                    ease: "",
                    onComplete: () => {
                        this.delta = 0;
                        this.size = 0;
                        this.osc.amp(0);
                    }
                }
                this.size = (randomGaussian() + 1) * 3;

                f = 300;
                t = "square";
                break;
            case 1: // line
                onHit = {
                    size: 1.0,
                    duration: 0.2,
                    ease: "elastic.in",
                    onComplete: () => {
                        this.size = 0;
                        this.osc.amp(0);
                    }
                }
                f = 1000*random();
                t = "triangle";
                break;
            case 2: // grid
                onHit = {
                    size: 5,
                    duration: 0.5,
                    ease: "elastic.in",
                    onComplete: () => {
                        this.size = 0;
                        this.osc.amp(0);
                    }
                }
                f = random() * 500 - 100;
                t = "sine";
                break;
            case 3: // dot
                if (options.amp != 0) this.osc.amp(0.05);
                onHit = {
                    size: 0,
                    duration: "0.1",
                    ease: "",
                    onComplete: () => {
                        this.osc.amp(0);
                    }
                }
                f = random() * 1000;
                f = f / (this.index + 1);
                t = "square";
                break;
            case 4:
                this.osc.amp(0);
            case 5:
                fill(100, 50);
                circle(width/2, height/2, width - random()*20);
                t = "sine";
            default:
                break;
        }
        this.osc.freq(f);
        this.osc.setType(t);
        gsap.to(this, onHit);
    }

    exit(MODE) {
        if (MODE == 2 && this.index > 100) return true;
        if ((MODE == 3 || MODE == 5) && this.index > 1) return true;

        return false;
    }

    draw(MODE) {
        if (this.exit(MODE)) return;

        switch (MODE) {
            case 0:
                this.drawDash();
                break;
            case 1:
                this.drawLine();
                break;
            case 2:
                this.drawGrid();
                break;
            case 3:
                fill(100, 50);
                noStroke();
                circle(width/2, height/2, 10 + 5*random());
                break;
            case 4:
                fill(100);
                rect(0, 0, width, height)
                break;
            default:
                break;
                
        }
    }

    drawDash() {
        if (this.size == 0) return;

        fill(100, this.delta % 100 + 20);
        noStroke();

        let d = abs(this.y - this.cy);
        d = d < 10 ? 50 : 0;

        const y = ceil(this.y / this.size) * this.size;

        if (this.size != 0) {
            for (let i = 2; i < (this.size + d) * 2; i++) {
                rect((this.x + this.delta + i * random() * 10) % width, y - this.size / 2, this.size);
            }
        }

        rect((this.x + this.delta + random() * 10) % width, y - this.size / 2, this.size);
    }

    drawLine() {
        strokeWeight(5);
        stroke(0);

        let x2 = this.x * (1 - this.size) + this.cx * this.size;
        let y2 = this.y * (1 - this.size) + this.cy * this.size;

        line(this.x, this.y, x2, y2);

        strokeWeight(1);
        stroke(100);
        if (this.size != 0) line(this.x, this.y, x2, y2);
    }

    drawGrid() {
        strokeWeight(1);
        stroke(100, 20);
        noFill();
        const d = dist(this.x, this.y, this.cx, this.cy);
        this.drawRect(this.x, this.y, this.size * d / 2 + this.index * random(), 0);
    }

    drawRect(x, y, s, i) {
        if (i > 2) return;

        let a = s / 4 + (5 - i) * 2 + random();
        let b = s / 2 + random() * 3;

        if (this.index % 2 == 0) {
            a = s / 8 + random() + 10;
        }

        this.drawRect(x - a, y - a, b, i + 1);
        this.drawRect(x + a, y - a, b, i + 1);
        this.drawRect(x + a, y + a, b, i + 1);
        this.drawRect(x - a, y + a, b, i + 1);

        rect(x - b, y - b, s);
    }

    setCenter(x, y) {
        this.cx = x;
        this.cy = y;
    }
}