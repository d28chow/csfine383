class Atoms {
    atoms;
    options;

    constructor() {
        this.atoms = [];
        this.options = {
            hitChance: 0,
            amp: 0.1,
            type: "sine",
            freq: 100,
            x: 0,
        };

        let filter = new p5.Reverb();
        let comp = new p5.Compressor();
        comp.threshold(-50);
        filter.chain(comp);
        filter.drywet(0.05);

        for (let i = 0; i < 500; i++) {
            this.atoms.push(new Atom(i, filter));
        }
    }

    draw() { this.atoms.forEach(atom => atom.draw(this.options.MODE)); }

    update() {
        if (random(1) < (this.options.hitChance/50) / frameRate()) {
            const cx = randomGaussian(width/2, width/8);
            const cy = randomGaussian(height/2, height/8);

            let tempOptions = this.options;
            tempOptions.hitChance = 501*frameRate();

            this.atoms.forEach((atom) => {
                atom.setCenter(cx, cy);
                atom.update(tempOptions);
            });
            return;
        }
    
        this.atoms.forEach(atom => atom.update(this.options));
    }
}